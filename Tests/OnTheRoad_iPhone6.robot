*** Settings ***
Documentation    Suite description
Resource  /Users/woloszka/PycharmProjects/ios_robot/Resources/otrLogin.robot
#Resource  /Users/woloszka/PycharmProjects/ios_robot/Resources/otrHCPs.robot
#Resource  /Users/woloszka/PycharmProjects/ios_robot/Resources/common.robot
Library  AppiumLibrary

*** Test Cases ***
Test title
    [Tags]    DEBUG
    Provided precondition
    When action
    Then check expectations

bpter007
    otrLogin.logowanie  bpter007  SFAtest

*** Variables ***
${REMOTE_URL}  http://127.0.0.1:4723/wd/hub      # URL to appium server
${REMOTE_URL2}  http://0.0.0.0:4724/wd/hub      # URL to appium server
#${REMOTE_URL}  http://0.0.0.0:4723/wd/hub      # URL to appium server
${PLATFORM_NAME}  iOS
${PLATFORM_VERSION}  11.2


#app for simulator
${APP_LOCATION}  /Users/woloszka/Library/Developer/Xcode/DerivedData/OnTheRoad-fhhslxprqzkjprbetemnulgyccew/Build/Products/Debug-iphonesimulator/OnTheRoadDEV.app

#app for real device
${APP_LOCATION_REAL}  /Users/woloszka/Library/Developer/Xcode/DerivedData/OnTheRoad-fhhslxprqzkjprbetemnulgyccew/Build/Products/Debug-iphoneos/OnTheRoadDEV.app

#udid for real device iPhone 7
${UDID_REAL_iphone7}  2de07b7b957924aa8046d8ece6ece51a49ccfcc2

${UDID_REAL_iphone6s}  1bd30705f7073dd0cd0ea31f84a0baec64e06597

#udid for simulator iPhone 6
${UDID}  BAF5A4FB-3ACD-4F3A-9F96-96399F58B9AA


${BUNDLE_ID}  com.rochedev.ontheroad.dev


*** Keywords ***
Provided precondition
    Setup system under test

Before
# configuration to start iOS 11, tests on real device iPhone6s
    Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=iPhone 6  app=${APP_LOCATION_REAL}  platformVersion=${PLATFORM_VERSION}  noReset=false  useNewWDA=true  udid=${UDID_REAL_iphone6s}  newCommandTimeout=200
