*** Settings ***
Library  AppiumLibrary
Resource  /Users/woloszka/PycharmProjects/ios_robot/Resources/PO/otrHCPsPage.robot


*** Variables ***
${REMOTE_URL}  http://127.0.0.1:4723/wd/hub      # URL to appium server
${REMOTE_URL2}  http://0.0.0.0:4724/wd/hub      # URL to appium server
#${REMOTE_URL}  http://0.0.0.0:4723/wd/hub      # URL to appium server
${PLATFORM_NAME}  iOS
${PLATFORM_VERSION}  11.2


#app for simulator
${APP_LOCATION}  /Users/woloszka/Library/Developer/Xcode/DerivedData/OnTheRoad-fhhslxprqzkjprbetemnulgyccew/Build/Products/Debug-iphonesimulator/OnTheRoadDEV.app

#app for real device
${APP_LOCATION_REAL}  /Users/woloszka/Library/Developer/Xcode/DerivedData/OnTheRoad-fhhslxprqzkjprbetemnulgyccew/Build/Products/Debug-iphoneos/OnTheRoadDEV.app

#udid for real device iPhone 7
${UDID_REAL_iphone7}  2de07b7b957924aa8046d8ece6ece51a49ccfcc2

#udid for real device iPhone 6
${UDID_REAL_iphone6s}  1bd30705f7073dd0cd0ea31f84a0baec64e06597

#udid for real device iPhone 7
${UDID_REAL}
${UDID_REAL}  2de07b7b957924aa8046d8ece6ece51a49ccfcc2

#udid for real device iPhone 6
${UDID_REAL}  1bd30705f7073dd0cd0ea31f84a0baec64e06597

#udid for simulator iPhone 6
${UDID}  BAF5A4FB-3ACD-4F3A-9F96-96399F58B9AA

${DEVICE_NAME}  iPhone 6
#${DEVICE_NAME}  iPhone 7


${BUNDLE_ID}  com.rochedev.ontheroad.dev

# noReset=true before test run there is no uninstall app. So if you wouldn't lost OTR session after log in set for true

*** Keywords ***
Before
# configuration to start iOS 11, tests on real device iPhone7
    #Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=iPhone 7  app=${APP_LOCATION_REAL}  platformVersion=${PLATFORM_VERSION}  noReset=false  useNewWDA=true  udid=${UDID_REAL_iphone7}  newCommandTimeout=200

# configuration to start iOS 11, tests on real device iPhone6s
    #Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=iPhone 6  app=${APP_LOCATION_REAL}  platformVersion=${PLATFORM_VERSION}  noReset=false  useNewWDA=true  udid=${UDID_REAL_iphone6s}  newCommandTimeout=200
    Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=${DEVICE_NAME}  app=${APP_LOCATION_REAL}  platformVersion=${PLATFORM_VERSION}  noReset=false  useNewWDA=false  udid=${UDID_REAL}  newCommandTimeout=200  fullReset=true

# configuration to start iOS 11, tests on simulator
    #Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=${DEVICE_NAME}  app=${APP_LOCATION}  platformVersion=${PLATFORM_VERSION}  noReset=false  useNewWDA=false  newCommandTimeout=200  #bundleId=${bundle_id}

    #Open Application  ${REMOTE_URL2}  platformName=${PLATFORM_NAME}  deviceName=${DEVICE_NAME2}  app=${APP_LOCATION}  platformVersion=${PLATFORM_VERSION}  noReset=true  useNewWDA=true  newCommandTimeout=200  #bundleId=${bundle_id}


    #Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  deviceName=${DEVICE_NAME}  app=${APP_LOCATION}  automationName=Appium  platformVersion=${PLATFORM_VERSION}  noReset=false  newCommandTimeout=200  bundleId=${bundle_id}  udid=${UDID}  xcodeOrgId=6N383MPR9W  CODE_SIGN_IDENTITY=CZ2A3AN2T2  ScreenOrientation=portrait

After
    Close All Applications

BeforeCase
    tap  ${tapBar HCPs}

AfterCase
    tap  ${tapBar HCPs}