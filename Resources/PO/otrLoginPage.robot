*** Settings ***
Library  AppiumLibrary

*** Variables ***
${Username}  id=UNIX ID
${Password}  id=PASSWORD
${Dont allow}   id=Don't Allow
${Allow}   id=Allow

#additional:
${tapBarHCPs}  id=HCPs
${windowSearch}  id=Search

*** Keywords ***
Insert Username
    [Arguments]  ${unixid}
    Wait Until Element Is Visible  ${Username}
    clear text  ${Username}
    input value  ${Username}  ${unixid}


Insert Password
    [Arguments]  ${pass}
    clear text  ${Password}
    input text  ${Password}  ${pass}

Click Login
    click button  Login


