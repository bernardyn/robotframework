*** Settings ***
Library  AppiumLibrary

*** Variables ***
#navigationBar
${navigationBar Route66}  xpath=//XCUIElementTypeNavigationBar[@name="HCPs"]/XCUIElementTypeButton[1]
#${navigationBar Route66Number}  xpath=//XCUIElementTypeStaticText[@name="2"]
${navigationBar HCPs}  xpath=//XCUIElementTypeButton[@name="HCPs"]
${navigationBar Add}  id=Add
${navigationBar Hide}  id=Hide
${window Search}  id=Search
${cell HCP phone}  id=phone dark
${cell HCP name}  id=Abril, Annette MD
${cell HCP address}  id=3 Sheridan Sq Kingsport, TN
${cell HCP no completed interactions}  id=No completed interactions in the last 90 days
${AccountDetails Back}  id=Back
${AccountDetails Profile Info}  id=Profile Info
${AccountDetails Ramp}  id=btn paperplane
${AccountDetails Interaction}  id=Interaction
${AccountDetails MedComm}  id=MedComm
${AccountDetails Referral}  id=MSL Referral
${Interaction Text}  id=Create Interaction
${Interaction Cancel}  id=Cancel
${Interaction Save}  id=Save
${Interaction AddProduct button}  id=Insert Add Discussed Product/Service
${Interaction AddProduct Label}  id=Add Discussed Product/Service
${Interaction Products Actemra button}  id=Insert Actemra
${Interaction Products Actemra label}  id=Actemra
${Interaction Products Actemra Del button}  id=Delete Actemra (Primary)
${Interaction Products Actemra Del label}  id=Actemra (Primary)
${Interaction Products Done}  id=Done
${Interaction Products Complete}  id=Complete Interaction
${Interaction Products Save in Progess}  id=Save in Progress
${Interaction Products Cancel}  id=Cancel



#kyeboard
${A}  name=A
${B}  name=B
${R}  name=R
${I}  name=I
${L}  name=L
${Search}  id=Search

#accounts list

#tapBar
${tapBar HCPs}  id=HCPs
${tapBar Favorites}  id=Favorites
${tapBar Activities}  id=Activities
${tapBar Notifications}  id=Notifications
${tapBar More}  id=More


*** Keywords ***
Interaction create
    tap  ${windowSearch}
    clear text  ${windowSearch}
    Input Abril
    click element  ${cell HCP name}
    click element  ${AccountDetails Interaction}
    click element  ${Interaction AddProduct button}
    click element  ${Interaction Products Actemra button}
    click element  ${Interaction Products Done}
    click element  ${Interaction Save}
    click element  ${Interaction Products Complete}
    page should contain text  ${AccountDetails Profile Info}

Input Abril
    input text  ${Search}  Abril

TabBar verification
    click element  ${tapBar HCPs}
    click element  ${tapBar Favorites}
    click element  ${tapBar Activities}
    tap  ${tapBar Notifications}
    tap  ${tapBar More}
    tap  ${tapBar HCPs}
    tap  ${tapBar Favorites}